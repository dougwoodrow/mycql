//
// Created by Doug Woodrow on 10/8/15.
//

#include "dataRecord.h"

DataRecord Record;

dataRecord::dataRecord() {
    Record = DataRecord();
}

/**
 * getCurrentRecord
 *
 * Returns the current data record being handled by the system
 *
 * @return DataRecord
 */
DataRecord getCurrentRecord() {
    return Record;
}

