//
// Created by Doug Woodrow on 10/8/15.
//

#ifndef MYCQL_DATASTRUCT_H
#define MYCQL_DATASTRUCT_H

#include <stdlib.h>

typedef struct DataRecord {
    int numeral;
    float decimal;
    char text[255];
} DataRecord;

class dataRecord {

    public:
        DataRecord getCurrentRecord();
        dataRecord();

};

#endif //MYCQL_DATASTRUCT_H
